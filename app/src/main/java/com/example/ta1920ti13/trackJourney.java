package com.example.ta1920ti13;

import android.Manifest;

import android.app.Activity;
import android.content.ContentResolver;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Location;
import android.net.Uri;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.core.content.FileProvider;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

import android.os.Environment;
import android.os.Handler;
import android.provider.MediaStore;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.MimeTypeMap;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.ta1920ti13.models.Coordinate;
import com.example.ta1920ti13.models.Media;
import com.example.ta1920ti13.models.Trip;
import com.example.ta1920ti13.models.User;
import com.example.ta1920ti13.ui.autentikasi.Login;
import com.example.ta1920ti13.ui.autentikasi.registrasi;
import com.example.ta1920ti13.ui.note.note;
import com.example.ta1920ti13.ui.voiceRecord;
import com.example.ta1920ti13.ui.yourLocation.yourLocation;
import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.EventListener;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.FirebaseFirestoreException;
import com.google.firestore.v1.WriteResult;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Timer;
import java.util.TimerTask;
import java.util.concurrent.Executor;

import static android.os.Environment.getExternalStoragePublicDirectory;

public class trackJourney extends Fragment {

    public static final int CAMERA_PERM_CODE = 101;
    public static final int CAMERA_REQUEST_CODE = 102;
    public static final int GALLERY_REQUEST_CODE = 105;
    public static final int REQUEST_VIDEO_CAPTURE = 1;
    public static final int TEN_MINUTES = 1000 * 60 * 10;

    ImageView cameraBtn, videoBtn, audioBtn, noteBtn;
    String currentPhotoPath;
    Date currentTime;

    SupportMapFragment supportMapFragment;
    FusedLocationProviderClient client;

    TextView finishBtn;

    LatLng latLng;
    MarkerOptions options;
    private ArrayList<LatLng> latlngs = new ArrayList<>();

    Trip trip = new Trip();
    ArrayList<Coordinate> allDataCoordinate = new ArrayList<>();
    ArrayList<Media> allDataMedia = null;

    Coordinate dataCoordinate;
    Media dataMedia;

    int MINUTES = 10;
    Timer timer = new Timer();


    FirebaseAuth fAuth;
    FirebaseFirestore fStore;
    public User user = home.user;
    public String userId;

    public trackJourney() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_track_journey, container, false);

        fAuth = FirebaseAuth.getInstance();
        fStore = FirebaseFirestore.getInstance();

        cameraBtn = view.findViewById(R.id.addPicture_button);
        videoBtn = view.findViewById(R.id.addVideo_button);
        audioBtn = view.findViewById(R.id.addAudio_button);
        noteBtn = view.findViewById(R.id.addNote_button);
        finishBtn = view.findViewById(R.id.finish_button);

        videoBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                askCameraVideoPermissions();
            }
        });

        cameraBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                askCameraPermissions();
            }
        });

        audioBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                openRecorder();
            }
        });

        noteBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                openNote();
            }
        });

        finishBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                kembaliKeHome();
            }
        });

        supportMapFragment = (SupportMapFragment) this.getChildFragmentManager()
                .findFragmentById(R.id.mapview);

        client = LocationServices.getFusedLocationProviderClient(getContext());

        if (ActivityCompat.checkSelfPermission(getContext(), Manifest.permission.ACCESS_FINE_LOCATION)
                == PackageManager.PERMISSION_GRANTED) {
            timer.schedule(new TimerTask() {
                @Override
                public void run() { // Function runs every MINUTES minutes.
                    getCurrentLocation();
                }
            }, 0, 1000 * 60 * MINUTES);
        }

        currentTime = Calendar.getInstance().getTime();
        trip.setStart_at(currentTime.toString());
        trip.setCreated_at(currentTime.toString());

        return view;
    }

    private void openNote() {
        Fragment newFragment = new note();
        FragmentTransaction transaction = getFragmentManager().beginTransaction();
        transaction.replace(R.id.nav_host_fragment, newFragment);
        transaction.commit();
    }

    private void kembaliKeHome() {
        currentTime = Calendar.getInstance().getTime();
        if(Login.userId == null){
            userId = user.getUserID();
        }

        trip.setName(user.getName());
        trip.setCreated_by(user.getName());
        trip.setDataCoordinate(allDataCoordinate);
        trip.setEnd_at(currentTime.toString());

        HashMap<String,Object> map = new HashMap<>();
        map.put("Name", trip.getName());
        map.put("Created_By", trip.getName());
        map.put("Created_At", trip.getCreated_at());
        map.put("Start_At", trip.getStart_at());
        map.put("End_At", trip.getEnd_at());

        DocumentReference documentReference2 = fStore.collection("user").document(userId)
                .collection("trip").document(trip.getName());
        documentReference2.set(map);
        
        timer.cancel();
        startActivity(new Intent(getContext(),home.class));
    }

    private void getCurrentLocation() {
        if (ActivityCompat.checkSelfPermission(getContext(), Manifest.permission.ACCESS_FINE_LOCATION)
                != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(getContext(), Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(getActivity(),
                    new String[]{Manifest.permission.ACCESS_FINE_LOCATION},44);
        }
        Task<Location> task = client.getLastLocation();
        task.addOnSuccessListener(new OnSuccessListener<Location>() {
            @Override
            public void onSuccess(final Location location) {
                if(location != null){
                    supportMapFragment.getMapAsync(new OnMapReadyCallback() {
                        @Override
                        public void onMapReady(GoogleMap googleMap) {
                            latLng = new LatLng(location.getLatitude(), location.getLongitude());
                            options = new MarkerOptions().position(latLng);
                            if(allDataMedia != null)
                            {
                                dataCoordinate.setDataMedia(allDataMedia);
                                allDataCoordinate.add(dataCoordinate);
                            }
                            dataCoordinate = new Coordinate();
                            currentTime = Calendar.getInstance().getTime();

                            dataCoordinate.setSequence("1");
                            dataCoordinate.setName(latLng.toString());
                            dataCoordinate.setTime(currentTime.toString());
                            allDataMedia = new ArrayList<>();
                            latlngs.add(latLng);

                            googleMap.animateCamera(CameraUpdateFactory.newLatLngZoom(latLng, 10));
                            googleMap.addMarker(options);
                        }
                    });
                }
            }
        });
    }

//    Handler mHandler = new Handler();
//    Runnable mHandlerTask = new Runnable(){
//        @Override
//        public void run() {
//            getCurrentLocation();
//            mHandler.postDelayed(mHandlerTask, TEN_MINUTES);
//        }
//    };

    private void openRecorder(){
        // Create new fragment and transaction
        Fragment newFragment = new voiceRecord();
        FragmentTransaction transaction = getFragmentManager().beginTransaction();

        // Replace whatever is in the fragment_container view with this fragment,
        // and add the transaction to the back stack if needed
        transaction.replace(R.id.nav_host_fragment, newFragment);

        // Commit the transaction
        transaction.commit();
    }

    private void askCameraVideoPermissions() {
        if(ContextCompat.checkSelfPermission(getContext(),Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED){
            ActivityCompat.requestPermissions((Activity) getContext(),new String[] {Manifest.permission.CAMERA}, CAMERA_PERM_CODE);
        }else {
            dispatchTakeVideoIntent();
        }

    }

    private void askCameraPermissions() {
        if(ContextCompat.checkSelfPermission(getContext(),Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED){
            ActivityCompat.requestPermissions((Activity) getContext(),new String[] {Manifest.permission.CAMERA}, CAMERA_PERM_CODE);
        }else {
            dispatchTakePictureIntent();
        }

    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        if(requestCode == CAMERA_PERM_CODE){
            if(grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED){
                dispatchTakePictureIntent();
            }else {
                Toast.makeText(getActivity(), "Camera Permission is Required to Use camera.", Toast.LENGTH_SHORT).show();
            }
        }
        if(requestCode == 44){
            if(grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED){
                getCurrentLocation();
            }
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        if(requestCode == CAMERA_REQUEST_CODE){
            if(resultCode == Activity.RESULT_OK){
                File f = new File(currentPhotoPath);

                Intent mediaScanIntent = new Intent(Intent.ACTION_MEDIA_SCANNER_SCAN_FILE);
                Uri contentUri = Uri.fromFile(f);
                mediaScanIntent.setData(contentUri);
                getActivity().sendBroadcast(mediaScanIntent);
            }

        }

        if(requestCode == GALLERY_REQUEST_CODE){
            if(resultCode == Activity.RESULT_OK){
                Uri contentUri = data.getData();
                String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
                String imageFileName = "JPEG_" + timeStamp +"."+getFileExt(contentUri);
            }
        }

        if (requestCode == REQUEST_VIDEO_CAPTURE && resultCode == Activity.RESULT_OK) {
            Uri videoUri = data.getData();
        }
    }

    private String getFileExt(Uri contentUri) {
        ContentResolver c = getActivity().getContentResolver();
        MimeTypeMap mime = MimeTypeMap.getSingleton();
        return mime.getExtensionFromMimeType(c.getType(contentUri));
    }

    private File createImageFile() throws IOException {
        // Create an image file name
        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
        String imageFileName = "JPEG_" + timeStamp + "_";
        File storageDir =   getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES);
        File image = File.createTempFile(
                imageFileName,  /* prefix */
                ".jpg",         /* suffix */
                storageDir      /* directory */
        );

        // Save a file: path for use with ACTION_VIEW intents
        currentPhotoPath = image.getAbsolutePath();

        currentTime = Calendar.getInstance().getTime();
        dataMedia = new Media();
        dataMedia.setName(imageFileName);
        dataMedia.setPath(storageDir.toString());
        dataMedia.setCreated_at(currentTime.toString());
        allDataMedia.add(dataMedia);

        return image;
    }

    private void dispatchTakePictureIntent() {
        Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        // Ensure that there's a camera activity to handle the intent
        if (takePictureIntent.resolveActivity(getActivity().getPackageManager()) != null) {
            // Create the File where the photo should go
            File photoFile = null;
            try {
                photoFile = createImageFile();
            } catch (IOException ex) {
                System.out.println(ex.fillInStackTrace());
            }
            // Continue only if the File was successfully created
            if (photoFile != null) {
                Uri photoURI = FileProvider.getUriForFile(getActivity(),
                        "com.example.ta1920ti13",
                        photoFile);

                takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT, photoURI);
                startActivityForResult(takePictureIntent, CAMERA_REQUEST_CODE);
            }
        }
    }

    private void dispatchTakeVideoIntent() {
        Intent takeVideoIntent = new Intent(MediaStore.ACTION_VIDEO_CAPTURE);
        if (takeVideoIntent.resolveActivity(getActivity().getPackageManager()) != null) {
            startActivityForResult(takeVideoIntent, REQUEST_VIDEO_CAPTURE);
        }
    }

    // untuk trackjourney
    public void swapYourLocation(View view) {
        // Create new fragment and transaction
        Fragment newFragment = new yourLocation();
        FragmentTransaction transaction = getFragmentManager().beginTransaction();

        // Replace whatever is in the fragment_container view with this fragment,
        // and add the transaction to the back stack if needed
        transaction.replace(R.id.nav_host_fragment, newFragment);

        // Commit the transaction
        transaction.commit();
    }


}
