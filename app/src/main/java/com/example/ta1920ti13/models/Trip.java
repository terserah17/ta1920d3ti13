package com.example.ta1920ti13.models;

import java.util.ArrayList;

public class Trip {
    String name, created_by, start_at, end_at, created_at;
    ArrayList<Coordinate> dataCoordinate;

    public  Trip(){}

    public Trip(String name, String created_by, String start_at, String end_at, String created_at, ArrayList<Coordinate> dataCoordinate) {
        this.name = name;
        this.created_by = created_by;
        this.start_at = start_at;
        this.end_at = end_at;
        this.created_at = created_at;
        this.dataCoordinate = dataCoordinate;
    }

    public String getName() {
        return name;
    }

    public String getCreated_by() {
        return created_by;
    }

    public String getStart_at() {
        return start_at;
    }

    public String getEnd_at() {
        return end_at;
    }

    public String getCreated_at() {
        return created_at;
    }

    public ArrayList<Coordinate> getDataCoordinate() {
        return dataCoordinate;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setCreated_by(String created_by) {
        this.created_by = created_by;
    }

    public void setStart_at(String start_at) {
        this.start_at = start_at;
    }

    public void setEnd_at(String end_at) {
        this.end_at = end_at;
    }

    public void setCreated_at(String created_at) {
        this.created_at = created_at;
    }

    public void setDataCoordinate(ArrayList<Coordinate> dataCoordinate) {
        this.dataCoordinate = dataCoordinate;
    }
}
