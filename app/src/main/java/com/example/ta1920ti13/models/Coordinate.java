package com.example.ta1920ti13.models;

import java.util.ArrayList;

public class Coordinate {
    String sequence;
    String time, name, reference;
    int number_of_photos, number_of_voice_recordings, number_of_videos, number_of_notes;
    ArrayList<Media> dataMedia;

    public Coordinate(){}

    public Coordinate(String sequence, String time, String name, String reference, int number_of_photos, int number_of_voice_recordings, int number_of_videos, int number_of_notes, ArrayList<Media> dataMedia) {
        this.sequence = sequence;
        this.time = time;
        this.name = name;
        this.reference = reference;
        this.number_of_photos = number_of_photos;
        this.number_of_voice_recordings = number_of_voice_recordings;
        this.number_of_videos = number_of_videos;
        this.number_of_notes = number_of_notes;
        this.dataMedia = dataMedia;
    }

    public String getSequence() {
        return sequence;
    }

    public void setSequence(String sequence) {
        this.sequence = sequence;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getReference() {
        return reference;
    }

    public void setReference(String reference) {
        this.reference = reference;
    }

    public int getNumber_of_photos() {
        return number_of_photos;
    }

    public void setNumber_of_photos(int number_of_photos) {
        this.number_of_photos = number_of_photos;
    }

    public int getNumber_of_voice_recordings() {
        return number_of_voice_recordings;
    }

    public void setNumber_of_voice_recordings(int number_of_voice_recordings) {
        this.number_of_voice_recordings = number_of_voice_recordings;
    }

    public int getNumber_of_videos() {
        return number_of_videos;
    }

    public void setNumber_of_videos(int number_of_videos) {
        this.number_of_videos = number_of_videos;
    }

    public int getNumber_of_notes() {
        return number_of_notes;
    }

    public void setNumber_of_notes(int number_of_notes) {
        this.number_of_notes = number_of_notes;
    }

    public ArrayList<Media> getDataMedia() {
        return dataMedia;
    }

    public void setDataMedia(ArrayList<Media> dataMedia) {
        this.dataMedia = dataMedia;
    }
}
