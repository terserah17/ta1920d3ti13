package com.example.ta1920ti13.models;

public class Media {
    String sequence;
    String path, name, created_at;

    public Media(){}

    public Media(String sequence, String path, String name, String created_at) {
        this.sequence = sequence;
        this.path = path;
        this.name = name;
        this.created_at = created_at;
    }

    public String getSequence() {
        return sequence;
    }

    public String getPath() {
        return path;
    }

    public String getName() {
        return name;
    }

    public String getCreated_at() {
        return created_at;
    }

    public void setSequence(String sequence) {
        this.sequence = sequence;
    }

    public void setPath(String path) {
        this.path = path;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setCreated_at(String created_at) {
        this.created_at = created_at;
    }
}
