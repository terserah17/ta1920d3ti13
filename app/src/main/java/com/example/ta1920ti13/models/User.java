package com.example.ta1920ti13.models;

public class User {
    private String email, nama, noHP, userID;

    public User(){}
    public User(String email, String nama, String noHP)
    {
        this.email = email;
        this.nama = nama;
        this.noHP = noHP;
    }

    public void setUserID(String userID){this.userID = userID;}
    public void setEmail(String email){this.email = email;}
    public void setName(String nama){this.nama = nama;}
    public void setNoHP(String noHP){this.noHP = noHP;}

    public String getName(){
        return nama;
    }
    public String getEmail(){
        return email;
    }
    public String getNoHP(){
        return noHP;
    }
    public String getUserID(){
        return userID;
    }


}
