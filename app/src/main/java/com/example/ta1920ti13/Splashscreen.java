package com.example.ta1920ti13;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.os.Handler;
import android.content.Intent;

import com.example.ta1920ti13.ui.autentikasi.registrasi;

public class Splashscreen extends AppCompatActivity {
    private int waktu_loading = 4000;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_splashscreen);

        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {

                Intent home=new Intent(Splashscreen.this, registrasi.class);
                startActivity(home);
                finish();

            }
        },waktu_loading);
    }
}
