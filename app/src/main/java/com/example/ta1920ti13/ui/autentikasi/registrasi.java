package com.example.ta1920ti13.ui.autentikasi;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.example.ta1920ti13.R;
import com.example.ta1920ti13.controller.Autentikasi;
import com.example.ta1920ti13.home;
import com.example.ta1920ti13.models.User;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.FirebaseFirestore;

import java.util.HashMap;
import java.util.Map;

public class registrasi extends AppCompatActivity {
    public static final String TAG = "TAG";
    EditText nRname, nRemail, nRphone, nRpass, nRtypepass;
    Button nRreg;
    TextView nLoginButton;
    ProgressBar progressBar;
    FirebaseAuth fAuth;
    FirebaseFirestore fStore;
    String userID;

    Autentikasi autentikasi = new Autentikasi();
    public static User user = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_registrasi);

        nRname = (EditText) findViewById(R.id.Rname);
        nRemail = (EditText) findViewById(R.id.Remail);
        nRphone = (EditText) findViewById(R.id.Rphone);
        nRpass = (EditText) findViewById(R.id.Rpass);
        nRtypepass = (EditText) findViewById(R.id.Rtypepass);
        nLoginButton = (TextView) findViewById(R.id.LoginButton);
        nRreg = (Button) findViewById(R.id.Rreg);

        fAuth = FirebaseAuth.getInstance();
        fStore = FirebaseFirestore.getInstance();
        progressBar = findViewById(R.id.Progressbar);

        if(fAuth.getCurrentUser() != null){
            startActivity(new Intent(getApplicationContext(), home.class));
            finish();
        }

        //ini kalau di tekan login
        nLoginButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(getApplicationContext(),Login.class));
            }
        });

        //ini untuk registrasi
        nRreg.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {
                String name, email, phone, pass, tpass;
                name = nRname.getText().toString().trim();
                email = nRemail.getText().toString().trim();
                phone = nRphone.getText().toString().trim();
                pass = nRpass.getText().toString().trim();
                tpass = nRtypepass.getText().toString().trim();

                if (TextUtils.isEmpty(email)) {
                    nRemail.setError("Please Input your email adress");
                    return;
                } else if (TextUtils.isEmpty(name)) {
                    nRname.setError("Please Input your full name");
                    return;
                } else if (TextUtils.isEmpty(phone)) {
                    nRphone.setError("Please Input your phone number");
                    return;
                } else if (TextUtils.isEmpty(pass)) {
                    nRpass.setError("Please Input your password");
                    return;
                } else if (pass.length() < 6) {
                    nRpass.setError("Password must be at least 6 characters long");
                    return;
                } else if (TextUtils.isEmpty(tpass)) {
                    nRtypepass.setError("Please Input your retype password");
                    return;
                } else if (pass.compareTo(tpass) != 0) {
                    nRtypepass.setError("Retype Passwords don't match");
                    return;
                }

                user = new User(email, name, phone);

                progressBar.setVisibility(View.VISIBLE);

                fAuth.createUserWithEmailAndPassword(email, pass)
                        .addOnCompleteListener(new OnCompleteListener<AuthResult>() {
                            @Override
                            public void onComplete(@NonNull Task<AuthResult> task) {
                                if(task.isSuccessful()) {
                                    Toast.makeText(registrasi.this, "User created.", Toast.LENGTH_SHORT).show();
                                    userID = fAuth.getCurrentUser().getUid();
                                    user.setUserID(userID);
                                    DocumentReference documentReference = fStore.collection("user").document(userID);
                                    documentReference.set(user).addOnSuccessListener(new OnSuccessListener<Void>() {
                                        @Override
                                        public void onSuccess(Void aVoid) {
                                            Log.d(TAG, "onSuccess: user profile is created for " + userID);
                                        }
                                    });
                                    startActivity(new Intent(getApplicationContext(),home.class));
                                }else{
                                    Toast.makeText(registrasi.this, "Register Unsuccessfully", Toast.LENGTH_SHORT)
                                            .show();
                                }
                            }
                        });
            }
        });
    }
}
