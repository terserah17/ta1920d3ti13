package com.example.ta1920ti13.ui.note;

import android.os.Bundle;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.TextView;

import com.example.ta1920ti13.R;
import com.example.ta1920ti13.trackJourney;
import com.example.ta1920ti13.ui.yourLocation.yourLocation;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.OutputStreamWriter;

public class note extends Fragment {

    TextView saveBtn, kembaliBtn;
    EditText judul, isi;
    String dataIsi, dataJudul, namaFile;

    public note() { }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_note, container, false);

        saveBtn = view.findViewById(R.id.simpanNote);
        kembaliBtn = view.findViewById(R.id.kembaliLagi);

        judul = view.findViewById(R.id.judulNote);
        isi = view.findViewById(R.id.isiNote);

        dataIsi = judul.getText().toString() + "\n" + isi.getText().toString();
        dataJudul = judul.getText().toString();

        saveBtn.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {
                writeToFile();
            }
        });
        kembaliBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                kembaliKeJourney();
            }
        });

        return view;
    }

    private void writeToFile() {
        try {
            OutputStreamWriter outputStreamWriter = new OutputStreamWriter(getContext().
                    openFileOutput(dataJudul+".txt", getContext().MODE_PRIVATE));
            outputStreamWriter.write(dataIsi);
            outputStreamWriter.close();
            kembaliKeJourney();
        } catch (FileNotFoundException e) {

        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void kembaliKeJourney() {
        // Create new fragment and transaction
        Fragment newFragment = new trackJourney();
        FragmentTransaction transaction = getFragmentManager().beginTransaction();

        // Replace whatever is in the fragment_container view with this fragment,
        // and add the transaction to the back stack if needed
        transaction.replace(R.id.nav_host_fragment, newFragment);

        // Commit the transaction
        transaction.commit();
    }

}