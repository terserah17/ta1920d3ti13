package com.example.ta1920ti13.ui.autentikasi;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.example.ta1920ti13.R;
import com.example.ta1920ti13.home;
import com.example.ta1920ti13.models.User;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;

public class Login extends AppCompatActivity {
    TextView nRegisterButton;
    EditText nLemail, nPasswd;
    Button nLogin;
    ProgressBar progressBar;
    FirebaseAuth fAuth;

    public static String userId = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        nLemail = (EditText) findViewById(R.id.Lemail);
        nPasswd = (EditText) findViewById(R.id.passwd);
        nLogin = (Button) findViewById(R.id.login);
        nRegisterButton = (TextView) findViewById(R.id.RegisterButton);

        fAuth = FirebaseAuth.getInstance();
        progressBar = findViewById(R.id.Progressbar);

        //ini kalau di tekan registrasi
        nRegisterButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(getApplicationContext(),registrasi.class));
            }
        });

        nLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String email,pass;

                email = nLemail.getText().toString().trim();
                pass = nPasswd.getText().toString().trim();

                if (TextUtils.isEmpty(email)) {
                    nLemail.setError("Please Input your email adress");
                    return;
                } else if (TextUtils.isEmpty(pass)) {
                    nPasswd.setError("Please Input your password");
                    return;
                } else if (pass.length() < 6) {
                    nPasswd.setError("Password must be at least 6 characters long");
                    return;
                }

                progressBar.setVisibility(View.VISIBLE);

                fAuth.signInWithEmailAndPassword(email,pass).addOnCompleteListener(new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        if(task.isSuccessful()) {
                            Toast.makeText(Login.this, "Login Successfully.", Toast.LENGTH_SHORT).show();
                            userId = fAuth.getCurrentUser().getUid();
                            startActivity(new Intent(getApplicationContext(),home.class));
                        }else{
                            Toast.makeText(Login.this, "Login Unsuccessfully", Toast.LENGTH_SHORT).show();
                        }
                    }
                });
            }
        });

    }
}
