package com.example.ta1920ti13.ui;

import android.app.Activity;
import android.content.pm.PackageManager;
import android.media.MediaRecorder;
import android.os.Bundle;
import android.Manifest;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.app.ActivityCompat;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;

import android.os.SystemClock;
import android.provider.MediaStore;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Chronometer;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.ta1920ti13.R;
import com.example.ta1920ti13.trackJourney;
import com.example.ta1920ti13.ui.yourLocation.yourLocation;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

public class voiceRecord extends Fragment{

    private static final int PERMISSION_CODE = 1;
    ImageView btnStart;
    TextView kembali;
    Chronometer timer;

    boolean isRecording = false;

    String recordPermission = Manifest.permission.RECORD_AUDIO;

    MediaRecorder mediaRecorder;

    String recordFile;

    public voiceRecord() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_voice_record, container, false);

        btnStart = view.findViewById(R.id.btnStart);
        timer = view.findViewById(R.id.record_timer);
        btnStart.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startRecorder();
            }
        });

        return view;
    }

    public void startRecorder(){
        if(isRecording){
            //stop;
            btnStart.setImageDrawable(getResources().getDrawable(R.drawable.start, null));
            stopRecording();
            isRecording = false;
        }else{
            //start;
            if(checkPermission()) {
                btnStart.setImageDrawable(getResources().getDrawable(R.drawable.stop, null));
                startRecording();
                isRecording = true;
            }
        }
    }

    private void startRecording() {
        timer.setBase(SystemClock.elapsedRealtime());
        timer.start();
        String recordPath = getActivity().getExternalFilesDir("/").getAbsolutePath();
        SimpleDateFormat format = new SimpleDateFormat("yyyy_mm_dd_hh_mm_ss", Locale.CANADA);
        Date now = new Date();
        recordFile = "Recording"+ format.format(now) +".3gp";
        mediaRecorder = new MediaRecorder();
        mediaRecorder.setAudioSource(MediaRecorder.AudioSource.MIC);
        mediaRecorder.setOutputFormat(MediaRecorder.OutputFormat.THREE_GPP);
        mediaRecorder.setOutputFile(recordPath+ "/" +recordFile);
        mediaRecorder.setAudioEncoder(MediaRecorder.AudioEncoder.AMR_NB);
        try {
            mediaRecorder.prepare();
        } catch (IOException e) {
            e.printStackTrace();
        }
        mediaRecorder.start();
    }

    private void stopRecording() {
        timer.stop();
        mediaRecorder.stop();
        mediaRecorder.release();
        mediaRecorder = null;
    }

    private boolean checkPermission(){
        if(ActivityCompat.checkSelfPermission(getContext(), Manifest.permission.RECORD_AUDIO) == PackageManager.PERMISSION_GRANTED){
            return true;
        }else{
            ActivityCompat.requestPermissions(getActivity(), new String[]{recordPermission}, PERMISSION_CODE);
            return false;
        }
    }
}